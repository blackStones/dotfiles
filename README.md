# Messy Dotfiles
[Manage your dotfiles with a bare repo](https://www.atlassian.com/git/tutorials/dotfiles)

The install script will clone this repo as a bare repo in in `$HOME/.dotfiles`.

Then it will modify system files in `/etc`, `/boot`, and `/usr` as well as your home folder. In the process it will place the readme, the license and itself in the root of your file system, together with a bunch of poorly organized files in `$HOME`.

**Do not run blindly!**

### TODO
- [ ] Clean Up
  - [ ] Refactor
  - [ ] Conglomerate
  - [ ] Document
- [ ] Move to ZSH
- [ ] Take care of XDG vars
- [ ] Add aliases

### Completed ✓
- [x] Get used to the mess I created.
